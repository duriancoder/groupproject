//
//  app.js
//  
//  Summary: Main entry point for the project
//  
//                  Changelog
//
//  Version     Author      Date        Comments
//  -------     ------      ----        --------
//  1.0         Hafiz       08/02/17    Creation of file
//  2.0         Sim CK      15/02/17    Data access via server (via Service Module)
//  2.1         Sim CK      16/02/17    Group 'All' moved to html file instead of in the group array
//  2.2			Hafiz		16/02/17	Modified data structure of Friend to include First/Last name
//  3.0         Hafiz       25/02/17    Major revision of app and file structure!
//  3.1         Hafiz       27/02/17    Addede a few more routes and multer in preparation for file upload
//


var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");


const NODE_PORT = process.env.NODE_PORT || 4000;
const CLIENT_FOLDER = path.join(__dirname, "../client");
const IMG_FOLDER = path.join(__dirname, "../uploads");
const MSG_FOLDER = path.join(__dirname, "../client/assets/messages");

const API_FRIENDS_ENDPOINT = "/api/friends";
const API_GROUPS_ENDPOINT = "/api/groups";

var app = express();

app.use(express.static(CLIENT_FOLDER));
app.use("/uploads", express.static(IMG_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Routes
require("./route")(app);

app.listen(NODE_PORT, function() {
    console.log("App started at port: %s", NODE_PORT);
});