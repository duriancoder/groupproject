
// 06 Mar 2017  Sim CK  Added references such that the tables are connected in SQL EED diagram

module.exports = function(conn, Sequelize) {

    var FriendMap = conn.define("friendmap", {

        friend_id: {

            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            /*references: {
                    model: 'friends',
                    key: 'friend_id'
            }*/

        },
        group_id: {

            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            /*references: {
                    model: 'groups',
                    key: 'group_id'
            }*/

        }
    }, {

        tableName: "friendmap",
        //timestamps: true

    });

    return FriendMap;
}