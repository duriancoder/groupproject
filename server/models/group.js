module.exports = function(conn, Sequelize) {

    var Groups = conn.define("groups", {

        group_id: {

            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            unique: true

        },
        group_owner: {

            type: Sequelize.INTEGER(11),
            allowNull: false

        },
        group_name: {

            type: Sequelize.STRING,
            allowNull: false

        },
        group_photo: {

            type: Sequelize.STRING,
            allowNull: false

        }
    }, {

        tableName: "groups",
        //timestamps: true

    });

    return Groups;
}