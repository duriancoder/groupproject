module.exports = function(conn, Sequelize) {

    var Friends = conn.define("friends", {

        friend_id: {

            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            unique: true

        },
        first_name: {

            type: Sequelize.STRING,
            allowNull: false

        },
        last_name: {

            type: Sequelize.STRING,
            allowNull: false

        },
        // Roger. Added name as per the HTML input field
        name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        // Roger changed name to be similar to friend controller name.
        friend_photo: {

            type: Sequelize.STRING,
            allowNull: true

        },
        // Roger to double check what data type should address be because address contains other propoerties within
        address: {
            type: Sequelize.STRING,
            allowNull: true
        },
        //Roger: to double check on the type
        email: {
            type: Sequelize.STRING,
            allowNull: true

        },
        gender: {

            type: Sequelize.ENUM('M', 'F'),
            allowNull: false
        },
        phone: {
            
            type: Sequelize.STRING,
            allowNull: false

        }
    }, {

        tableName: "friends",
        //timestamps: true

    });

    return Friends;
}