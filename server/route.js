var Sequelize = require("sequelize");
var fs = require("fs");
var multer = require("multer");
var storage = multer.diskStorage({
    destination: "./uploads",                                                       // <-- Location to save file.
    filename: function(req, file, callback) {
        callback(null, Math.round(+new Date()/1000) + "-" + file.originalname);     // <-- Format of file saved.
    }
});
var upload = multer({
    storage: storage
});


const API_FRIENDS_ENDPOINT = "/api/friends";
const API_GROUPS_ENDPOINT = "/api/groups";
const API_UPLOAD_ENDPOINT = "/api/upload";
const API_FRIENDMAP_ENDPOINT = "/api/friendmap"
const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "1CapitalH";

// Create mySQL connection
var connection = new Sequelize(
    'gabdb',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

// Connection auth and sync
connection.authenticate(function(){
    console.log("DB connected");
}).catch(function(err){
    console.log(err);
});


// Load db models
var Friend = require("./models/friend")(connection, Sequelize);
var Group = require("./models/group")(connection, Sequelize);
var Map = require("./models/friendmap")(connection, Sequelize);

// Table relations
Map.hasOne(Friend, {foreignKey: "friend_id"});
Group.hasMany(Map, {foreignKey: "group_id"});


module.exports = function(app) {
    /***
     * COMPLETED:-------------------------------------------------------------------------
     * GET      /api/friends                -- Get all friends
     * GET      /api/friends/g[group_id]    -- Get all friends belonging in [group_id]
     * GET      /api/friends/f[friend_id]   -- Get [friend_id] details
     * GET      /api/groups                 -- Get all groups
     * GET      /api/groups/f[friend_id]    -- Get all groups that the friend belongs to
     * GET      /api/groups/g[group_id]     -- Get [group_id] details
     * 
     * POST     /api/groups                 -- Create new group
     * POST     /api/friends                -- Create new friend
     * POST     /api/upload                 -- Upload image file
     * POST     /api/friendmap              -- Create new friendmap
     * 
     * DELETE   /api/groups/g[group_id]     -- Delete all fields related to group_id
     * DELETE   /api/friends/f[friend_id]   -- Delete all fields related to friend_id
     * 
     * PUT      /api/groups/g[group_id]     -- Edit [group_id] details 
     * PUT      /api/groups/f[group_id]     -- Add friend to group
     * PUT      /api/friends/f[friend_id]   -- Edit [friend_id] details 
     * -----------------------------------------------------------------------------------
     */

    //  GET     /api/friends
    app.get(API_FRIENDS_ENDPOINT, function(req, res) {
        console.log("GET friends");

        Friend
            .findAll()
            .then(function(friends) {
                res
                    .status(200)
                    .json(friends);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });
    });


    //  GET     /api/friends/g[group_id]
    app.get(API_FRIENDS_ENDPOINT + "/g:group_id", function(req, res) {
        console.log("GET friends of group %s", req.params.group_id);

        if(req.params.group_id == "all") {
            console.log("Getting all friends");
            Friend
                .findAll()
                .then(function(friends) {
                    res
                        .status(200)
                        .json(friends);
                })
                .catch(function(err) {
                    res
                        .status(500)
                        .json(err);
                });
        }
        else {
            Group
                .findAll({
                    where: {
                        group_id: req.params.group_id
                    },
                    include: [{
                        model: Map,
                        include: [Friend]
                    }]
                })
                .then(function(groups) {
                    var friendList = [];
                    for(var i in groups[0].friendmaps) {
                        friendList.push(groups[0].friendmaps[i].friend);    // <-- store in friendList ONLY the friend information of that particualr group_id
                    }
                    res
                        .status(200)
                        .json(friendList);
                })
                .catch(function(err) {
                    res
                        .status(500)
                        .json(err);
                });
        }    
    });


    //  GET     /api/friends/f[friend_id]
    app.get(API_FRIENDS_ENDPOINT + "/f:friend_id", function(req, res) {
        console.log("GET friend of friend_id: %s", req.params.friend_id);

        Friend
            .findAll({
                where: {
                    friend_id: req.params.friend_id
                }
            })
            .then(function(friend) {
                res
                    .status(200)
                    .json(friend);

            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });
    });


    //  GET     /api/groups
    app.get(API_GROUPS_ENDPOINT, function(req, res) {
        console.log("GET groups");

        Group
            .findAll()
            .then(function(groups) {
                res
                    .status(200)
                    .json(groups);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });
    });


    //  GET     /api/groups/f[friend_id]
    app.get(API_GROUPS_ENDPOINT + "/f:friend_id", function(req, res) {
        console.log("GET groups of friend_id: %s", req.params.friend_id);

        Group
            .findAll({
                include: [{
                    model: Map,
                    where: {
                        friend_id: req.params.friend_id
                    }
                }]
            })
            .then(function(groups) {
                res
                    .status(200)
                    .json(groups);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });

    });

    //  GET     /api/groups/g[group_id]
    app.get(API_GROUPS_ENDPOINT + "/g:group_id", function(req, res) {
        console.log("GET groups of group_id: %s", req.params.group_id);

        Group
            .findAll({
                where: {
                    group_id: req.params.group_id
                }
            })
            .then(function(group) {
                res
                    .status(200)
                    .json(group);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });
    });

    //  POST    /api/groups
    app.post(API_GROUPS_ENDPOINT, function(req, res) {
        console.log("POST groups");

        /***
         * Function looking for this JSON format:
         * {
         *      "group": {
         *          "owner": 1,
         *          "name": "group name",
         *          "photo": "group photo url" 
         *      }
         * }
         */

        Group
            .create({
                group_owner: req.body.group.owner,
                group_name: req.body.group.name,
                group_photo: req.body.group.photo
            })
            .then(function(group) {
                res
                    .status(200)
                    .json(group);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });
    });


    //  POST    /api/friends
    app.post(API_FRIENDS_ENDPOINT, function(req, res) {
        console.log("POST friends");

        /***
         * Function looking for this JSON format:
         * {
         * 
         *      "friend": {
         *          "first_name": "first name"
         *          "last_name": "last name"
         *          "friend_photo": "friend photo url",
         *          "gender": "M",
         *          "phone": "1234567"
         *      }
         *
         * }
         */
         // updated with a few more properties..
        Friend
            .create({
                first_name: req.body.friend.first_name,
                last_name: req.body.friend.last_name,
                name: req.body.friend.name,
                email: req.body.friend.email,
                gender: req.body.friend.gender,
                address: req.body.friend.address,
                phone: req.body.friend.phone,
                friend_photo: req.body.friend.friend_photo
            })
            .then(function(friend) {
                res
                    .status(200)
                    .json(friend);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });
    });


    //  POST    /api/upload
    app.post(API_UPLOAD_ENDPOINT, upload.single("img-file"), function(req, res) {
        console.log("Uploading image...");
        fs.readFile(req.file.path, function(err, data) {
            if(err) {
                console.log("Error: " + err);
            }
            res
                .status(202)
                .json({
                    size: req.file.size,
                    name: req.file.filename
                });
        });
    });


    //  DELETE  /api/groups/g[group_id]
    app.delete(API_GROUPS_ENDPOINT + "/g:group_id", function(req, res) {
        // Delete record where [group_id] matches the parameter
        Group
            .destroy({
                where: {
                    group_id: req.params.group_id
                }
            })
            .then(function(group) {
                // When record successfully deleted from groups table,
                // delete all records which has [group_id] in friendmap
                // to remove all lingering association between friend and group
                Map
                    .destroy({
                        where: {
                            group_id: req.params.group_id
                        }
                    })
                    .then(function(map) {
                        res
                            .status(200)
                            .json({status: "Group delete successful"});

                    })
                    .catch(function(err){
                        res
                            .status(500)
                            .json(err);
                    });
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });


    });

    //  POST    /api/friendmap
    app.post(API_FRIENDMAP_ENDPOINT + "/:friend_id/:group_id", function(req, res) {
        console.log("POST friendmap");

        Map
            .create({
                friend_id: req.params.friend_id,
                group_id: req.params.group_id
            })
            .then(function(map) {
                res
                    .status(202)
                    .json(map);
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            })
    });

    //  DELETE  /api/friends/f[friend_id]
    app.delete(API_FRIENDS_ENDPOINT + "/f:friend_id", function(req, res) {
        // Delete record where [group_id] matches the parameter
        Friend
            .destroy({
                where: {
                    friend_id: req.params.friend_id
                }
            })
            .then(function(group) {
                // When record successfully deleted from groups table,
                // delete all records which has [group_id] in friendmap
                // to remove all lingering association between friend and group
                Map
                    .destroy({
                        where: {
                            friend_id: req.params.friend_id
                        }
                    })
                    .then(function(map) {
                        res
                            .status(200)
                            .json({status: "Friend delete successful"});

                    })
                    .catch(function(err){
                        res
                            .status(500)
                            .json(err);
                    });
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            });


    });


    app.put(API_GROUPS_ENDPOINT + "/g:group_id", function(req, res) {
        // Update group records
        Group
            .update({
                group_owner: req.body.group.owner,
                group_name: req.body.group.name,
                group_photo: req.body.group.photo
            }, {
                where: {
                    group_id: req.params.group_id
                }
            })
            .then(function(group) {
                res
                    .status(200)
                    .json({status: "Group update successful"});
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            })
    });

    // Add friend to group (inside Map) /api/groups/f[group_id]
    app.post(API_GROUPS_ENDPOINT + "/f:group_id", function(req, res) {
            
        // console.log("APP.POST req.body"+req.body.friend_id);

        Map
            .create({
                    friend_id: req.body.friend_id,
                    group_id: req.params.group_id
            })
            .then(function(group) {
                res
                    .status(200)
                    .json({status: "Friend added to Group update successful"});
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            })
    });

    // Search friend based on first or last name /api/friends/search/?searchString
    app.get("/api/friends/search", function (req, res) {
        Friend
        // Use findAll to retrieve multiple records
            .findAll({
                // Use the where clause to filter final result; e.g. with searchString in last name or first name
                where: {
                    // $or operator tells sequelize to retrieve record that match any of the condition
                    $or: [
                        // $like + % tells sequelize that matching is not a strict matching, but a pattern match
                        // % allows you to match any string of zero or more characters
                        {last_name: {$like: "%" + req.query.searchString + "%"}},
                        {first_name: {$like: "%" + req.query.searchString + "%"}}
                    ]
                }
            })
            // this .then() handles successful findAll operation
            // in this example, findAll() used the callback function to return friends
            .then(function (friends) {
                res
                    .status(200)
                    .json(friends);
            })
            // this .catch() handles erroneous findAll operation
            .catch(function (err) {
                res
                    .status(500)
                    .json(err);
            });
    });

    // Update friend record    /api/friends/f[friend_id]
    app.put(API_FRIENDS_ENDPOINT + "/f:friend_id", function(req, res) {
        // Update friend record
        console.log("APP.PUT edit Friend req.body"+JSON.stringify(req.body.friend));

        Friend
            .update({
                first_name: req.body.friend.first_name,
                last_name: req.body.friend.last_name,
                name: req.body.friend.name,
                email: req.body.friend.email,
                gender: req.body.friend.gender,
                address: req.body.friend.address,
                phone: req.body.friend.phone,
                friend_photo: req.body.friend.friend_photo
            }, {
                where: {
                    friend_id: req.params.friend_id
                }
            })
            .then(function(group) {
                res
                    .status(200)
                    .json({status: "Friend update successful"});
            })
            .catch(function(err) {
                res
                    .status(500)
                    .json(err);
            })
    });
}


/***
 * Lines commented below are for developmentt purposes
 * to sync the database tables and fill up the tables
 * with hard-coded sample values for testing.
 */

// connection.sync({
//     force:true
// }).then( function(){
//     console.log("Sync db!");

//     // Once database is synced, fill up tables with some hard-coded values first 
//     var fArray = [];
//     fArray.push({
//         first_name: "Hafiz",
//         last_name: "Misron",
//         name: "Fiz",
//         email: "hafiz.misron@gmail.com",
//         address: "Somewhere in Singapore",
//         friend_photo: "https://ig-s-d-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e35/13259085_641292556020623_1526322651_n.jpg",
//         gender: "M",
//         phone: "1234567"
//     });
//     fArray.push({
//         first_name: "Roger",
//         last_name: "Chow",
//         friend_photo: "https://ig-s-c-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/s480x480/e35/13534310_1796541783925562_1134046167_n.jpg",
//         gender: "M",
//         phone: "1234567"        
//     });
//     fArray.push({
//         first_name: "Cher Khern",
//         last_name: "Sim",
//         friend_photo: "https://ig-s-d-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e35/13385620_143385209408047_646029286_n.jpg",
//         gender: "M",
//         phone: "1234567"        
//     });https://hafizmisron@bitbucket.org/hafizmisron/fsf17_day20_assessment.git
//     fArray.push({
//         first_name: "Christian",
//         last_name: "Nugraha",
//         name: "Chris",
//         email: "nugraha@sg.ibm.com",
//         friend_photo: "https://ig-s-b-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e35/13108690_933068443480009_414924341_n.jpg",
//         gender: "M",
//         phone: "1234567"        
//     });
//     fArray.push({
//         first_name: "Yong Hong",
//         last_name: "Ng",
//         friend_photo: "https://ig-s-c-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e35/11371041_586249818181134_915858395_n.jpg",
//         gender: "M",
//         phone: "1234567"        
//     });
//     fArray.push({
//         first_name: "Hisham",
//         last_name: "Misron",
//         friend_photo: "https://ig-s-a-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/s750x750/sh0.08/e35/13534307_665542416936904_131979514_n.jpg",
//         gender: "M",
//         phone: "1234567"        
//     });
//     fArray.push({
//         first_name: "Hafizah",
//         last_name: "Misron",
//         friend_photo: "https://ig-s-c-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e15/1169853_1399007737004562_1918204165_n.jpg",
//         gender: "F",
//         phone: "1234567"        
//     });
//     fArray.push({
//         first_name: "Misron",
//         last_name: "Arib",
//         friend_photo: "https://ig-s-d-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e15/1389324_514640341977531_1774643298_n.jpg",
//         gender: "M",
//         phone: "1234567"        
//     });
//     fArray.push({
//         first_name: "Mariam",
//         last_name: "Mohd",
//         friend_photo: "https://ig-s-a-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e35/14269125_1592967674339620_367311976885387264_n.jpg",
//         gender: "F",
//         phone: "1234567"        
//     });


//     var gArray = [];
//     gArray.push({
//         group_owner: 1,
//         group_name: "IBM",
//         group_photo: "https://ig-s-c-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e15/11007754_942601662419782_1375608372_n.jpg"        
//     });
//     gArray.push({
//         group_owner: 1,
//         group_name: "ISS",
//         group_photo: "https://ig-s-d-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e35/16585107_1705988549691283_5885900066647965696_n.jpg"        
//     });
//     gArray.push({
//         group_owner: 1,
//         group_name: "Family",
//         group_photo: "https://ig-s-b-a.akamaihd.net/hphotos-ak-xat1/t51.2885-15/e35/13652202_319521651716089_1967528223_n.jpg"        
//     });


//     var mArray = [];
//     mArray.push({
//         friend_id: 1,
//         group_id: 1
//     });
//     mArray.push({
//         friend_id: 1,
//         group_id: 2
//     });
//     mArray.push({
//         friend_id: 1,
//         group_id: 3
//     });
//     mArray.push({
//         friend_id: 2,
//         group_id: 2
//     });
//     mArray.push({
//         friend_id: 3,
//         group_id: 2
//     });
//     mArray.push({
//         friend_id: 4,
//         group_id: 1
//     });
//     mArray.push({
//         friend_id: 4,
//         group_id: 2
//     });
//     mArray.push({
//         friend_id: 5,
//         group_id: 1
//     });
//     mArray.push({
//         friend_id: 5,
//         group_id: 2
//     });
//     mArray.push({
//         friend_id: 6,
//         group_id: 3
//     });
//     mArray.push({
//         friend_id: 7,
//         group_id: 3
//     });
//     mArray.push({
//         friend_id: 8,
//         group_id: 3
//     });
//     mArray.push({
//         friend_id: 9,
//         group_id: 3
//     });


//     // Friend Table
//     for (var i in fArray) {
//         Friend
//             .create(fArray[i]);
//     }


//     // Group Table
//     for(var j in gArray) {
//         Group
//             .create(gArray[j]);
//     }


//     // Friendmap Table
//     for(var k in mArray) {
//         Map
//             .create(mArray[k]);
//     }

// });