# **GLORIFIED ADDRESS BOOK (gab)** #

Group project for Full Stack Foundation class, by DurianCoder

## **Completed Functions** ##
* Front Page: List All Friends and All Groups
* Front Page: List Friends belonging to a particular Group
* Front Page: List of Friends dynamically updated using $scope and $rootScope
* Front Page: Display Group name and number of Friends in Group
* Front Page: Search function to look for Friend
* Front Page: Mobile-friendly
* Add Friend: Create and store Friend info in database
* Add Friend: Upload Friend photo to server
* Add Friend: List all Groups and allow Friend to be associated to any particular Group
* View Friend: Display Friend info from database
* View Friend: Delete Friend will remove Friend from Friends and all associated records in Friendmap tables
* Edit Friend: Update Friend details
* Add Group: Create and store Group info in database
* Add Group: Upload Group photo to server
* Edit Group: Edit and store Group info in database
* Edit Group: Uploads new photo only if new photo is selected
* Delete Group: Deletes Group from Groups table, and all associated records in Friendmap table.

Bonus:
* Drag and Drop Friend into Group

##** Contributions** ##
### Hafiz ###
#### Initial version ####
-  Initial project files setup/creation
-  UI layout design
-  Pop up dialog
#### Database version ####
-  Port over to database storage
    - Create schema
    - Modularization
-  Add group functionality
-  Edit group functionality
-  Delete functionality 

### Sim CK ###
#### Initial version ####
- Client/Server link with hard-coded data
- Groups/Friends display mechanism 
- Search functionality 
#### Database version ####
- Edit friend functionality
- Search functionaliy
- Drag and Drop between 2 controllers
    - Add friend to group functionality

### Roger ###
- Add friend functionality



### What is this repository for? ###

* Assignment: My Friends App
* Functionality
*   - Add Friend
*   - Edit Friend’s contact information
*   - Group friends
*   - List all friends
*   - Delete Friends - Unfriend
*   - Share friend’s contact
*   - Create grouping for your friends
*   - Search friend’s name first name or last name
*   - Chat with your friends (BONUS)
*   - Show different and differentiate the group of friends
*   - Drag and drop rearrange your friends into different group (BONUS)
* 
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Muhammad Hafiz Bin Misron
* Sim Cher Khern
* Roger Chow

### Friend Object ###

This is what Friend object should tentatively look like:

    var Friend = {
        first_name: [first_name],
        last_name: [last_name],
        full_name:   
            function() {
                return this.first_name + "" + this.last_name;
            },
        photo: [photo_url],
        group: [group_array],               // One Friend can belong in multiple groups
        status: [status],                   // status: online or offline
        contact_info:   
            var contact_info {
                email: [email],
                phone: [phone_number],
                social: [social_media]
            }
    }