(function () {
    angular
        .module("GAB")
        .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['$scope', '$rootScope', 'GABService'];

    function SearchDBCtrl($scope, $rootScope, GABService) {
        var vm = this;

        
        init();

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.searchFriendDB = searchFriendDB;
        vm.selectGroup = selectGroup;


        // Listen to whether button was clicked
        $scope.$on("searchFriend", function(event, args){
            vm.searchString = args.searchString;
   		    console.log("Search string: " + vm.searchString);
            vm.searchFriendDB(vm.searchString);
	    });

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        // init();

        // // The init function initializes view
        
        function init() {
            vm.result = null;
            vm.showSearch = false;
            vm.friendsList=[];
            vm.groupsList = [];
            vm.searchString="";
        }

        // The search function searches for departments that matches query string entered by user. The query string is
        // matched against the department name and department number alike.
        function searchFriendDB() {
            vm.showSearch = true;

            GABService
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .searchFriendDB(vm.searchString)
                .then(function (results) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    vm.friendsList = results.data;
                    console.log("friendsList: "+ JSON.stringify(vm.friendsList));

                    vm.groupsList=[];

                    GABService
                        .getGroups()
                        .then(function(groups) {
                            vm.groupsList = groups;
                            console.log("groupsList: "+ JSON.stringify(vm.groupsList));
                        })
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
        }

        function selectGroup(group_id) {
            console.log("group selected: " + group_id);
            $rootScope.$broadcast('updateFriends', {id: group_id});
        }

    }
})();