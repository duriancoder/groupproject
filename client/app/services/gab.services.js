(function () {

    angular
        .module("GAB")
        .service("GABService", GABService);
    
    GABService.$inject = ["$http", "Upload"];

    function GABService($http, Upload) {
        var service = this;

        
        // Expose the functions
        service.getFriends = getFriends;
        service.getGroups = getGroups;
        service.getFriend = getFriend;
        service.addGroup = addGroup;
        service.uploadImage = uploadImage;
        service.deleteFriend = deleteFriend;
        service.deleteGroup = deleteGroup;
        service.addFriend = addFriend; /*added by Roger Chow*/
        service.getGroup = getGroup;
        service.updateGroup = updateGroup;
        service.createMap = createMap;
        service.addFriendtoGroup = addFriendtoGroup;
        service.searchFriendDB = searchFriendDB;
        service.updateFriend = updateFriend;
        
        // Interface with server with $http GET/POST/PUT/DELETE etc.
        function getFriends(group_id) {
            console.log("getFriends() in services accessed.");
            return ($http.get("/api/friends/g" + group_id)
                .then(function(result) {
                    return(result.data);
                }));
        }

        //function as a service to add new friend details, added by Roger Chow
        function addFriend(friend){
            console.log(friend);
            return $http({
                method: 'POST',
                url: "/api/friends",
                data: {friend: friend}
            });
        }

        function getGroups(friend_id) {
            console.log("getGroups() in services accessed.");
            // No params = get all groups
            if(!friend_id) {
                return ($http.get("/api/groups")
                    .then(function(result) {
                        return(result.data);
                    }));
            }
            // With friend_id, get all groups based on friend_id
            else {
                console.log("friend_id:"+friend_id);

                return ($http.get("/api/groups/f" + friend_id)
                    .then(function(result) {
                        console.log("result:"+result.data);
                        return(result.data);
                    }));
            }
        }

        function getGroup(group_id) {
            console.log("getGroup() in services accessed.");
            return ($http.get("/api/groups/g" + group_id)
                .then(function(result) {
                    return result.data;
                }));
        }

        function getFriend(friend_id) {
            console.log("getFriend() in services accessed.");
            return($http.get("/api/friends/f" + friend_id)
                .then(function(result) {
                    return(result.data);
                }));
        }

        function addGroup(group_data) {
            console.log("addGroup() in services accessed.");
            return($http.post("/api/groups", group_data)
                .then(function(result) {
                    return result.data;
                }));
        }

        function uploadImage(img_data) {
            console.log("uploadImage() in services accessed.");
            /*** img_data should look like this:
             * url: "/upload",
             * data: {
             *      "img-file": vm.imgFile
             * }
             */

            img_data.url = "/api/upload";
            return(Upload.upload(img_data)
                .then(function(result) {
                    return result.data;
                }));
        }

        function deleteFriend(friend_id) {
            return($http.delete("/api/friends/f" + friend_id)
                .then(function(result) {
                    return result.data;
                }));
        }

        function deleteGroup(group_id) {
            return($http.delete("/api/groups/g" + group_id)
                .then(function(result) {
                    return result.data;
                }));
        }

        function updateGroup(group_id, group_data) {
            return($http.put("/api/groups/g" + group_id, group_data)
                .then(function(result) {
                    return result.data;
                }));
        }

        function createMap(friend_id, group_id) {
            return($http.post("/api/friendmap/" + friend_id + "/" + group_id)
                .then(function(result) {
                    return result.data;
                }));
        }

        function addFriendtoGroup(group_id, friend_id) {
            return($http.post("/api/groups/f" + group_id, friend_id)
                .then(function(result) {
                    return result.data;
                }));
        }

        // searchFriendDB search for last name or first name that is same as searchString
        // string (params) Parameters: searchString. Returns: Promise object
        function searchFriendDB(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/friends/search'
                , params: {
                    'searchString': searchString
                }
            });
        }

        function updateFriend(friend_data) {
            return($http.put("/api/friends/f" + friend_data.friend.friend_id, friend_data)
                .then(function(result) {
                    return result.data;
                }));
        }

    }

})();