(function() {
    angular
        .module("GAB")
        .controller("MainCtrl", MainCtrl);


    MainCtrl.$inject = ['$window', "$rootScope", "GABService"];


    function MainCtrl($window, $rootScope, GABService) {
        var vm = this;
        vm.buttonClick = buttonClick;
        vm.searchString ="";

        function buttonClick(button, id) {
            switch(button) {

                case "home":
                    console.log("Logo clicked");
                    forceRefresh();
                    break;

                case "addfriend":
                    console.log("Add Friend clicked");
                    clearAll();
                    vm.addFriendVisible = true;
                    break;
                
                case "addgroup":
                    console.log("Add Group clicked");
                    clearAll();
                    vm.addGroupVisible = true;
                    break;
                
                case "viewfriend":
                    console.log("Viewing friend id: " + id);
                    clearAll();
                    vm.viewFriendVisible = true;
                    $rootScope.$broadcast('viewFriend', {id: id});
                    break;
                
                case "viewgroup":
                    console.log("Viewing group id: " + id);
                    clearAll();
                    vm.friendsVisible = true;
                    vm.groupsVisible = true;
                    $rootScope.$broadcast('updateFriends', {id: id});
                    break;
                
                case "editgroup":
                    console.log("Editing group id: " + id);
                    clearAll();
                    vm.editGroupVisible = true;
                    $rootScope.$broadcast('editGroup', {id: id});
                    break;

                case "searchFriend":
                    console.log("Search Friend: ");
                    clearAll();
                    vm.searchVisible = true;
                    $rootScope.$broadcast('searchFriend', {searchString: vm.searchString});
                    break;

                case "editFriend":
                    console.log("Edit Friend");
                    clearAll();
                    vm.editFriendVisible = true;
                    $rootScope.$broadcast('editFriend', {friend: id});

            }
        }

        function forceRefresh() {
            $window.location.reload();
        }

        function clearAll() {
            vm.friendsVisible = false;
            vm.groupsVisible = false;
            vm.addFriendVisible = false;
            vm.addGroupVisible = false;
            vm.viewFriendVisible = false;
            vm.editFriendsVisible = false;
            vm.editGroupVisible = false;
            vm.searchVisible = false;
        }

        function init() {
            // Main page: friends list and groups list visible
            vm.friendsVisible = true;
            vm.groupsVisible = true;
        }

        clearAll();
        init();

    }

})();