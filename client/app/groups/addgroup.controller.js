(function() {
    angular
        .module("GAB")
        .controller("AddGroupCtrl", AddGroupCtrl);
    
    AddGroupCtrl.$inject = ["$window", "GABService"];

    function AddGroupCtrl($window, GABService) {
        var vm = this;

        init();
        vm.result = "";
        vm.submit = submit;
       

        /***
         * Function looking for this JSON format:
         * {
         *      "group": {
         *          "owner": 1,
         *          "name": "group name",
         *          "photo": "group photo url" 
         *      }
         * }
         */

        function submit() {
            console.log("submit clicked");

            // No action if group name is empty
            if (vm.groupName=="") $window.location.reload();

            var img_data = {
                data: {
                    "img-file": vm.imgFile
                }
            }

            // Attempt to upload image first...
            GABService
                .uploadImage(img_data)
                .then(function(result) {
                    // If upload is successful...
                    console.log(result);
                    vm.fileURL = "./uploads/" + result.name;

                    var group_data = {
                        "group": {
                            "owner": 1,
                            "name": vm.groupName,
                            "photo": vm.fileURL
                        }
                    }

                    GABService
                        .addGroup(group_data)
                        .then(function(result) {
                            // If db update is successful...
                            console.log(result);
                            console.log("addGroup() success");
                            vm.result = result.group_name + " group added successfully!";
                            init();

                        })
                        .catch(function(err) {
                            // If db update is unsuccessful...
                            console.log(err);
                        });

                })
                .catch(function(err) {
                    // If upload is unsuccessful...
                    console.log(err);
                });

        }

        function init() {
            vm.groupName = "";
            vm.fileName = "";
            vm.imgFile = null;
            vm.fileURL = "";
        }
    }

})();