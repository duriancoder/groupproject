(function() {
    angular
        .module("GAB")
        .controller("EditGroupCtrl", EditGroupCtrl);
    
    EditGroupCtrl.$inject = ["$window", "$scope", "GABService"];

    function EditGroupCtrl($window, $scope, GABService) {
        var vm = this;
        vm.group_name = "";
        vm.group_photo = "";
        vm.result = "";
        vm.update = update;
        vm.imgFile = null;

        $scope.$on("editGroup", function(event, args){
            vm.group_id = args.id;
   		    console.log("editing group: " + vm.group_id);
            
            GABService
                .getGroup(vm.group_id)
                .then(function(group) {
                    console.log(group);
                    vm.group_name = group[0].group_name;
                    vm.group_photo = group[0].group_photo;                  
                });
	    });

        function update() {
            // Update group here
            console.log("Update button clicked");

            if(vm.imgFile) {
                var img_data = {
                    data: {
                        "img-file": vm.imgFile
                    }
                }

                GABService
                    .uploadImage(img_data)
                    .then(function(result) {
                        // Image upload successful.
                        console.log(result);
                        vm.fileURL = "./uploads/" + result.name;

                        var group_data = {
                            "group": {
                                "owner": 1,
                                "name": vm.group_name,
                                "photo": vm.fileURL
                            }
                        }

                        GABService
                            .updateGroup(vm.group_id, group_data)
                            .then(function(group) {
                                console.log(group);
                                vm.result = "Group updated!";
                            });
                    })
                    .catch(function(err) {
                        console.log(err);
                    });

            }
            else {
                console.log("No file to upload.");
                var group_data = {
                        "group": {
                            "owner": 1,
                            "name": vm.group_name,
                            "photo": vm.group_photo
                        }
                    }

                    GABService
                        .updateGroup(vm.group_id, group_data)
                        .then(function(group) {
                            console.log(group);
                            vm.result = "Group updated!";
                        });
            }
        } 
    }

})();