(function() {
    angular
        .module("GAB")
        .controller("GroupCtrl", GroupCtrl);

    GroupCtrl.$inject = ["$window", "$scope", "$rootScope", "GABService"];

    function GroupCtrl($window, $scope, $rootScope, GABService) {
        var vm = this;

        vm.listGroups = [];
        vm.friendGroups = [];

		vm.getGroups = getGroups;
        vm.selectGroup = selectGroup;
        vm.dropCallback = dropCallback;

        vm.dragFriend = {};
        vm.dropGroupID = 0;

        $scope.$on("updateGroups", function(event, args){
            vm.getGroups();
	    });

        $scope.$on("friendDrag", function(event, args){
            console.log("args :"+JSON.stringify(args));
            vm.dragFriend=args;
	    });


       function getGroups() {
            console.log("getGroups() in controller accessed.");

			GABService.getGroups()
				.then(function(allGroups) {
					vm.listGroups = allGroups;
				});
        }

        function selectGroup(group_id) {
            console.log("group selected: " + group_id);
            $rootScope.$broadcast('updateFriends', {id: group_id});
        }

        function addfriendtoGroup() {
            // data in vm.dropGroup and vm.dragfriend

            var x=0;

            console.log("Drag friend id:" + vm.dragFriend.friend_id);

            GABService
                .getGroups(vm.dragFriend.friend_id)
                .then(function (groups) {
                    vm.friendGroups = groups;
                    console.log("friendGroups length:" + vm.friendGroups.length);

                    for (var i in vm.friendGroups)
                        if (vm.friendGroups[i] == vm.DropGroupID) x++;

                    if (x==0) performAddFriendtoGroup();
                });

        }

        function performAddFriendtoGroup() {

            console.log("Perform add friend to group");

            var friend_id = {
                        "friend_id": vm.dragFriend.friend_id
                    };

            GABService
                .addFriendtoGroup(vm.dropGroupID, friend_id)
                .then(function (result) {
                    console.log("add friendtoGroup result"+result);
                });
        }

        function dropCallback(event, ui, element) {
          console.log('Dropped viewall' , element);
            vm.dropGroupID = element.group_id;
            console.log('Drop Grp ID: '+ vm.dropGroupID);

            addfriendtoGroup();
        };

        vm.getGroups();

    }

})();