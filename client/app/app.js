(function () {
    angular
        .module("GAB", [
            "ngMessages", 
            "ngAnimate",
            "ngFileUpload",
            "ngDragDrop"
        ]);
})();