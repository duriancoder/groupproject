(function() {
    angular
        .module("GAB")
        .controller("AddFriendCtrl", AddFriendCtrl);
    
    AddFriendCtrl.$inject = ["$window", "GABService"];

    function AddFriendCtrl($window, GABService) {
        var vm = this;
        vm.imgFile = null;
        vm.friend = {
            last_name: "lastname",
            first_name: "firstname",
            name: "name",
            email: "email",
            gender: "M",
            address: "Address",
            phone: "1234567",
            friend_photo: "url"
           
           /* home_address: {
                street: "",
                Blk: "",
                unit: "",
                postal: "",
            },
            tel: {
                home: "",
                office: "",
                Mobile: "",
            }*/
        };
        vm.groups = null;
        vm.selection = [];

        vm.response = "";

        vm.save = save;
        
        getGroups();


        function getGroups() {
            GABService
                .getGroups()
                .then(function(result) {
                    vm.groups = result;
                })
                .catch(function(err) {
                    vm.response = JSON.stringify(err);
                });
        }


        function save() {
            console.log("save friend contact...");
            console.log(vm.selection);
                            

            var img_data = {
                data: {
                    "img-file": vm.imgFile
                }
            }

            // Upload file first
            GABService
                .uploadImage(img_data)
                .then(function(img) {
                    // When upload is successful
                    vm.fileURL = "./uploads/" + img.name;
                    vm.friend.friend_photo = vm.fileURL;

                    // Update database with friend info
                    GABService
                        .addFriend(vm.friend)
                        .then(function(friend){
                            console.log("Friend ID: " + friend.data.friend_id);
                            vm.response = "Friend added!";
                            
                            for(var i=0; i<vm.selection.length; i++) {
                                if(vm.selection[i]) {
                                    console.log("friend_id: " + friend.data.friend_id + " group_id: " + i);
                                    GABService
                                        .createMap(friend.data.friend_id, i)
                                        .then(function(map) {
                                            console.log(map);
                                        })
                                        .catch(function(err) {
                                            console.log("error " + JSON.stringify(err));
                                            vm.response = JSON.stringify(err);
                                        });

                                }
                            }

                        }).catch(function(err){
                            console.log("error " + JSON.stringify(err));
                            vm.response = JSON.stringify(err);
                            // vm.status.message = err.data.name;
                            // vm.status.code = err.data.parent.errno;
                        });

                })
                .catch(function(err) {
                    // When upload is NOT successful
                    console.log("error " + JSON.stringify(err));
                    vm.response = JSON.stringify(err);
                });
        }
    }
})();