(function() {
    angular
        .module("GAB")
        .controller("EditFriendCtrl", EditFriendCtrl);
    
    EditFriendCtrl.$inject = ["$window", "$scope", "GABService"];

    function EditFriendCtrl($window, $scope, GABService) {
        var vm = this;

        vm.friend={};
        vm.listGroups = [];
        vm.imgFile = null;

        vm.performSave = performSave;
        vm.cancel = cancel;

        // Listen to whether button was clicked
        $scope.$on("editFriend", function(event, args){
            vm.friend = args.friend;
   		    console.log("Edit Friend: " + vm.friend);
            getFriendGroups();
	    });


        function performSave() {

            if (vm.imgFile==null) {
                console.log("Image file = NULL");
                saveWithoutImageUpload();
            }
            else {

                console.log("Update with new Image");

                var img_data = {
                data: {
                    "img-file": vm.imgFile
                    }
                }

                // Upload file first
                GABService
                    .uploadImage(img_data)
                    .then(function(result) {
                        // When upload is successful
                        vm.fileURL = "./uploads/" + result.name;
                        vm.friend.friend_photo = vm.fileURL;

                        saveWithoutImageUpload();
                    })
                    .catch(function(err) {
                        // When upload is NOT successful
                        console.log("Upload error " + JSON.stringify(err));
                        vm.response = JSON.stringify(err);
                    });
            }
        }


        function getFriendGroups() {

            GABService
                .getGroups(vm.friend.friend_id)
                .then(function(groups) {
                    vm.listGroups = groups;
                })

        }

        function saveWithoutImageUpload() {

             var friend_data = {
                        "friend": vm.friend
                    };

            console.log("vm.friend -- saving without photo:" + JSON.stringify(vm.friend));

            GABService
                .updateFriend(friend_data)
                .then(function(result) {
                    // When edit is done, go back to main screen
                    $window.location.reload();
                    // console.log("vm.friend -- saving without photo: successful");
                })
                .catch(function(err) {
                        // When upload is NOT successful
                        console.log("Update Friend unsuccessful " + JSON.stringify(err));
                        vm.response = JSON.stringify(err);
                    });
        }

        function cancel() {
            $window.location.reload();
        }
    }

})();