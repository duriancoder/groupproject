(function() {
    angular
        .module("GAB")
        .controller("ViewFriendCtrl", ViewFriendCtrl);
    
    ViewFriendCtrl.$inject = ["$window", "$scope", "GABService"];

    function ViewFriendCtrl($window, $scope, GABService) {
        var vm = this;

        vm.friend = {};
        vm.listGroups = {};
        vm.deleteFriend = deleteFriend;

        $scope.$on("viewFriend", function(event, args){
   		    console.log("Friend selected: " + args.id);
            getFriend(args.id);
	    });

        function deleteFriend(friend_id) {
            GABService
                .deleteFriend(friend_id)
                .then(function(result) {
                    // When delete is successful, go back to main screen
                    $window.location.reload();
                });
        }

        function getFriend(friend_id) {
            console.log("getFriend() in controller accessed.");

			GABService
                .getFriend(friend_id)
				.then(function(friend) {
					vm.friend = friend;
				});
            
            GABService
                .getGroups(friend_id)
                .then(function(groups) {
                    vm.listGroups = groups;
                })
		}
    }

})();