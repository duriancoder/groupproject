(function(){
    angular
        .module("GAB")
        .controller("FriendCtrl", FriendCtrl);

    FriendCtrl.$inject = ["$window", "$scope", "$rootScope", "GABService"];

    function FriendCtrl($window, $scope, $rootScope, GABService) {
        var vm = this;

        vm.selectedGroup = "all";  // default to all when page loads...

        vm.listFriends = [];
        vm.groupOptionsVisible = false;
        vm.group_id = 0;

        // Listen to whether button was clicked
        $scope.$on("updateFriends", function(event, args){
            vm.group_id = args.id;
   		    console.log("group selected: " + vm.group_id);
            vm.getFriends(vm.group_id);
	    });

		vm.getFriends = getFriends;
        vm.selectFriend = selectFriend;
        vm.deleteGroup = deleteGroup;
        vm.startCallback = startCallback;


        function getFriends(group_id) {
            console.log("getFriends() in controller accessed.");

            console.log("group_id "+group_id);

			GABService
                .getFriends(group_id)
				.then(function(allFriends) {
					vm.listFriends = allFriends;
				});

            if(group_id != "all") {
                vm.groupOptionsVisible = true;
                GABService
                    .getGroups()
                    .then(function(allGroups) {
                        console.log("allGroups:");
                        console.log(allGroups);
                        for(var i in allGroups) {
                            if(allGroups[i].group_id == group_id) {
                                vm.selectedGroup = allGroups[i].group_name;
                            }
                        }
                    });
            }
            else {
                vm.groupOptionsVisible = false;
                vm.selectedGroup = "all";
            }
		}

        function selectFriend(friend_id) {
            for(var i in vm.listFriends) {
                if(vm.listFriends[i].friend_id == friend_id) {
                    console.log("Selected: ");
                    console.log(vm.listFriends[i].first_name + " " + vm.listFriends[i].last_name);
                }
            }
        }

        function deleteGroup(group_id) {
            GABService
                .deleteGroup(group_id)
                .then(function(result) {
                    // After group is deleted,
                    // set the selected group to all
                    // and refresh the friends screen
                    vm.selectedGroup = "all";
                    vm.group_id = 0;
                    vm.getFriends(vm.selectedGroup);

                    // refresh the groups screen too...
                    $rootScope.$broadcast("updateGroups");
                });
        }


        function startCallback(event, ui, element) {
            console.log('Drag start', element);
            $rootScope.$broadcast('friendDrag', element);
        };


        // Fill the vm with all friends at first.
        vm.getFriends(vm.selectedGroup);

    }

})();